---
step: 77
message:
input:
  id: tf_6244763208860
  type: select
  label: What is the nature of your Customer portal question?
  options:
  - step: 84
    value: lnr_cdot_login
    name: Login related issues
  - step: 85
    value: lnr_cdot_transfer
    name: Account transfer related issues
  - step: 86
    value: lnr_cdot_contacts
    name: Adding approved contacts
  - step: 87
    value: lnr_cdot_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 75
  classes: []
- name: next
  value: Next
  step: 84
  classes: []
- name: next
  value: Next
  step: 85
  classes: []
- name: next
  value: Next
  step: 86
  classes: []
- name: next
  value: Next
  step: 87
  classes: []
---
