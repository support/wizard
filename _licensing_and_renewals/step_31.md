---
step: 31
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    You can find information on starting a trial <a href='https://about.gitlab.com/pricing/licensing-faq/#how-do-i-start-a-trial' target='_blank'>here</a>.
    <br />
    <br />
    Do note a GitLab trial can only be done once for each nameaspace. You can find more information on what happens after your trial ends <a href='https://about.gitlab.com/free-trial/#what-happens-after-my-free-trial-ends' target='_blank'>here</a>.
    <br />
    <br />
    If you are looking for a possible extension or to get more information about GitLab trials, you might want to contact our sales team.
    </div>
- type: input
  input_type: 'button'
  id: step_31_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 31
  link: https://about.gitlab.com/free-trial/
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_31_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 31
  link: https://about.gitlab.com/free-trial/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 7
  classes: []
---
