---
step: 75
message:
input:
  id: tf_6244777789084
  type: select
  label: What area does this pertain to?
  options:
  - step: 76
    value: lnr_category_purchasing_issues
    name: Making a purchase
  - step: 77
    value: lnr_category_cdot
    name: Questions/Issues about the Customer portal
  - step: 78
    value: lnr_category_sm
    name: Self-managed license related matters
  - step: 79
    value: lnr_category_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 4
  classes: []
- name: next
  value: Next
  step: 76
  classes: []
- name: next
  value: Next
  step: 77
  classes: []
- name: next
  value: Next
  step: 78
  classes: []
- name: next
  value: Next
  step: 79
  classes: []
---
