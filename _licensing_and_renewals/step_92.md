---
step: 92
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    As of 2022-07-07, GitLab has moved to Strict Cloud Licensing. This means that all customers utilize cloud activation keys instead of license files unless they qualify for an exemption. If you believe you qualify, please contact your GitLab Sales representative to discuss this further. You may also use the contact sales form if you are unsure who your sales representative is.
    </div>
- type: input
  input_type: 'button'
  id: step_92_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 92
  link:
  vars:
  value: Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_92_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 92
  link: N/A
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 78
  classes: []
---
