---
step: 29
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-warning
  content: |
    <div><img src='assets/images/warning.svg'></div>
    Click "Submit a ticket" to open the new ticket form. Note that some of the form fields will be pre-populated based on your answers so far.
- type: input
  input_type: 'button'
  id: step_29_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 29
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_cdot
  - tf_6244763208860=lnr_cdot_contacts
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I wish add other contacts to my subscription in the Customer portal
  - tf_description=<blockquote><p>Zendesk Wizard Notes</p><p>I have used the wizard and confirmed a ticket is required to resolve this issue.</p></blockquote>
  value: Submit a ticket
input:
buttons:
- name: previous
  value: Previous
  step: 26
  classes: []
---
