---
step: 24
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    To link your subscription to your namespace, you would need to first associate the <a href='https://customers.gitlab.com/customers/sign_in' target='_blank'>customers portal</a> account with the GitLab.com account (more info on that can be found <a href='https://docs.gitlab.com/ee/subscriptions/#change-the-linked-account' target='_blank'>here</a>). Then you would need to associate the GitLab.com group with the purchased subscription (more info on that can be found <a href='https://docs.gitlab.com/ee/subscriptions/#change-the-linked-namespace' target='_blank'>here</a>).
    <br />
    <br />
    For this to work, you will need to currently be an Owner of the namespace/group in question. If you are not, the current Owner of your namespace/group might need to either <a href='https://docs.gitlab.com/ee/user/group/manage.html#change-the-owner-of-a-group' target='_blank'>change the owner of a group</a> or <a href='https://docs.gitlab.com/ee/user/group/manage.html#add-users-to-a-group' target='_blank'>re-add your user to the group as an Owner</a>.
    </div>
- type: input
  input_type: 'button'
  id: step_24_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 24
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_subject=I need to link my subscription to my namespace
  - tf_6244777789084=lnr_category_saas
  - tf_6244816806044=lnr_saas_link_sub
  - tf_6244823221788=resold_status_no
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/</li><li>I have confirmed I am an owner of the namespace in question</li></ul></blockquote>
  value: I am still unable to link the subscription. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_24_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 24
  link: https://docs.gitlab.com/ee/subscriptions/
  vars:
  value: I have linked the subscription. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 18
  classes: []
---
