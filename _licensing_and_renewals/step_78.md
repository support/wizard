---
step: 78
message:
input:
  id: tf_6244817812892
  type: select
  label:  What is the nature of your Self-managed license question?
  options:
  - step: 88
    value: lnr_sm_did_not_receive
    name: I have not received my license
  - step: 89
    value: lnr_sm_license_errors
    name: I am getting errors applying my license or activation code
  - step: 90
    value: lnr_sm_seat_usage
    name: Seat usage related matters
  - step: 91
    value: lnr_sm_resend_to_new_contact
    name: I need my license sent to someone else
  - step: 92
    value: lnr_sm_cloud_opt_out
    name: Request to opt out of cloud licensing
  - step: 93
    value: lnr_sm_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 75
  classes: []
- name: next
  value: Next
  step: 88
  classes: []
- name: next
  value: Next
  step: 89
  classes: []
- name: next
  value: Next
  step: 90
  classes: []
- name: next
  value: Next
  step: 91
  classes: []
- name: next
  value: Next
  step: 92
  classes: []
- name: next
  value: Next
  step: 93
  classes: []
---
