---
step: 18
message:
input:
  id: tf_6244823221788
  type: select
  label: How was the subscription purchased?
  options:
  - step: 22
    value: resold_status_resold
    name: I purchased via an authorized partner or reseller (including AWS and GCP)
  - step: 23
    value: resold_status_reseller
    name: I am an authorized partner or reseller
  - step: 24
    value: resold_status_no
    name: I purchased directly from GitLab
  - step: 25
    value: resold_status_unsure
    name: I am not sure
buttons:
- name: previous
  value: Previous
  step: 6
  classes: []
- name: next
  value: Next
  step: 22
  classes: []
- name: next
  value: Next
  step: 23
  classes: []
- name: next
  value: Next
  step: 24
  classes: []
- name: next
  value: Next
  step: 25
  classes: []
---
