---
step: 76
message:
input:
  id: tf_6244764805916
  type: select
  label: What is the nature of your purchasing question?
  options:
  - step: 80
    value: lnr_purchasing_issues_cc_issues
    name: I am encountering credit card errors when trying to make a purchase
  - step: 81
    value: lnr_purchasing_issues_non_cc_issues
    name: How is it determining the number of seats I need to purchase?
  - step: 82
    value: lnr_purchasing_issues_other
    name: I have questions about buying a Self-Managed subscription
  - step: 83
    value: lnr_purchasing_issues_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 75
  classes: []
- name: next
  value: Next
  step: 80
  classes: []
- name: next
  value: Next
  step: 81
  classes: []
- name: next
  value: Next
  step: 82
  classes: []
- name: next
  value: Next
  step: 83
  classes: []
---
