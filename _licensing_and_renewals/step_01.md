---
step: 1
message:
input:
  id: tf_360012882099
  type: select
  label: What type of product are you working with?
  options:
  - step: 2
    value: l_and_r_product_type_saas
    name: GitLab.com (SaaS)
  - step: 3
    value: l_and_r_product_type_sm
    name: Self-Managed
  - step: 4
    value: l_and_r_product_type_gitlab-dedicated
    name: GitLab Dedicated
buttons:
- name: next
  value: Next
  step: 2
  classes: []
- name: next
  value: Next
  step: 3
  classes: []
- name: next
  value: Next
  step: 4
  classes: []
---
