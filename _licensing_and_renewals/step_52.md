---
step: 52
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about our self-managed subscriptions can be found on our <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html' target='_blank'>GitLab self-managed subscription</a> page. However, if you need more assistance, you might want to contact our sales team.
    </div>
- type: input
  input_type: 'button'
  id: step_52_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 52
  link:
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_52_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 52
  link: https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 43
  classes: []
---
