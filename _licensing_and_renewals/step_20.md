---
step: 20
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    This could be because the subscription linked to your namespace/group is incorrect. We would normally suggest trying to <a href='https://docs.gitlab.com/ee/subscriptions/#change-the-linked-namespace' target='_blank'>changing the linked namespace</a> on the subscription to ensure it is using the correct one.
    </div>
- type: input
  input_type: 'button'
  id: step_20_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 20
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_saas
  - tf_6244816806044=lnr_saas_sub_issues
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I need assistance in regards to seat usage on my namespace
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/gitlab_com/</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_20_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 20
  link: https://docs.gitlab.com/ee/subscriptions
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 6
  classes: []
---
