---
step: 46
message:
input:
  id: tf_6244817812892
  type: select
  label:  What is the nature of your Self-managed license question?
  options:
  - step: 69
    value: lnr_sm_did_not_receive
    name: I have not received my license
  - step: 70
    value: lnr_sm_license_errors
    name: I am getting errors applying my license or activation code
  - step: 71
    value: lnr_sm_seat_usage
    name: Seat usage related matters
  - step: 72
    value: lnr_sm_resend_to_new_contact
    name: I need my license sent to someone else
  - step: 73
    value: lnr_sm_cloud_opt_out
    name: Request to opt out of cloud licensing
  - step: 74
    value: lnr_sm_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 69
  classes: []
- name: next
  value: Next
  step: 70
  classes: []
- name: next
  value: Next
  step: 71
  classes: []
- name: next
  value: Next
  step: 72
  classes: []
- name: next
  value: Next
  step: 73
  classes: []
- name: next
  value: Next
  step: 74
  classes: []
---
