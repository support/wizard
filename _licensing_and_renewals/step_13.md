---
step: 13
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    A GitLab SaaS subscription uses a concurrent (seat) model. You pay for a subscription according to the maximum number of users assigned to the top-level group or its children during the billing period. You can add and remove users during the subscription period, as long as the total users at any given time doesn’t exceed the subscription count.
    <br />
    <br />
    More information on how we determine seat usage can be found <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined' target='_blank'>here</a>.
    <br />
    <br />
    Some other useful information for you might be <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#view-seat-usage' target='_blank'>viewing seat usage</a>, <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#export-seat-usage' target='_blank'>exporting seat usage</a>, and <a href='https://docs.gitlab.com/ee/api/members.html#list-all-billable-members-of-a-group' target='_blank'>listing billable members of a group via the API</a> (keep in mind our API uses <a href='https://docs.gitlab.com/ee/api/index.html#pagination' target='_blank'>pagination</a>, so you might need to make multiple calls if your usage is over the standard output length).
    </div>
- type: input
  input_type: 'button'
  id: step_13_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 13
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_purchasing_issues
  - tf_6244764805916=lnr_purchasing_issues_other
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I need information on my namespace's billable members
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/gitlab_com/</li><li>I have checked https://docs.gitlab.com/ee/api/members.html</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_13_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 13
  link: https://docs.gitlab.com/ee/subscriptions/gitlab_com/
  vars:
  value: This gave me the info I needed!
input:
buttons:
- name: previous
  value: Previous
  step: 5
  classes: []
---
