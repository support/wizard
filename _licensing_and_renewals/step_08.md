---
step: 8
message:
input:
  id: tf_6244802892956
  type: select
  label: What is the nature of your EDU/OSS program question?
  options:
  - step: 35
    value: lnr_special_programs_questions
    name: I have questions about the EDU/OSS program
  - step: 36
    value: lnr_special_programs_renew
    name: I need to renew/change my EDU/OSS subscription
  - step: 37
    value: lnr_special_programs_sheerid
    name: I am unable to claim my subscription using my SheerID
  - step: 38
    value: lnr_special_programs_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 2
  classes: []
- name: next
  value: Next
  step: 35
  classes: []
- name: next
  value: Next
  step: 36
  classes: []
- name: next
  value: Next
  step: 37
  classes: []
- name: next
  value: Next
  step: 38
  classes: []
---
