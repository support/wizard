---
step: 12
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    If your namespace/group is not showing in the dropdown, this would normally mean you do not have Owner level membership to the  namespace/group in question.
    <br />
    <br />
    This being the case, the current Owner of your namespace/group might need to either <a href='https://docs.gitlab.com/ee/user/group/manage.html#change-the-owner-of-a-group' target='_blank'>change the owner of a group</a> or <a href='https://docs.gitlab.com/ee/user/group/manage.html#add-users-to-a-group' target='_blank'>re-add your user to the group as an Owner</a>.
    </div>
- type: input
  input_type: 'button'
  id: step_12_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 12
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_purchasing_issues
  - tf_6244764805916=lnr_purchasing_issues_non_cc_issues
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I cannot see my namespace in the dropdown when making a purchase
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/user/group/manage.html</li><li>I have confirmed I am an owner of the namespace in question</li></ul></blockquote>
  value: This did not address my issue. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_12_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 12
  link: https://docs.gitlab.com/ee/user/group/manage.html
  vars:
  value: This has been resolved!
input:
buttons:
- name: previous
  value: Previous
  step: 5
  classes: []
- name: next
  value: Next
  step: 18
  classes: []
- name: next
  value: Next
  step: 19
  classes: []
- name: next
  value: Next
  step: 20
  classes: []
- name: next
  value: Next
  step: 21
  classes: []
---
