---
step: 28
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    To transfer ownership of the Customer portal account to someone else, you would want to first ensure the new person is an Owner of your group/namespace. If they are not, the current Owner of your namespace/group might need to either <a href='https://docs.gitlab.com/ee/user/group/manage.html#change-the-owner-of-a-group' target='_blank'>change the owner of a group</a> or <a href='https://docs.gitlab.com/ee/user/group/manage.html#add-users-to-a-group' target='_blank'>re-add your user to the group as an Owner</a>.
    <br />
    <br />
    After ensuring the new person is an Owner of the group/namespace, you would then want to <a href='https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information' target='_blank'>change the account owner's information</a> in the Customer portal. This would effectively transfer ownership of the account to the new person.
    </div>
- type: input
  input_type: 'button'
  id: step_28_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 28
  link: https://docs.gitlab.com/ee/subscriptions/
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_cdot
  - tf_6244763208860=lnr_cdot_transfer
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I need to transfer ownership of my customer portal account
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have tried changing the owner using the info at https://docs.gitlab.com/ee/subscriptions/</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_28_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 28
  link: https://docs.gitlab.com/ee/subscriptions/
  vars:
  value: I was able to change the owner of the account. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 26
  classes: []
---
