---
step: 15
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about CI Minutes can be found on the following pages:
    <ul>
      <li><a href='https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html' target='_blank'>CI/CD minutes quota</a></li>
      <li><a href='https://about.gitlab.com/pricing/faq-consumption-cicd/' target='_blank'>Managing CI/CD Minutes FAQ</a></li>
    </ul>
    However, if you need more assistance, you might want to contact our sales team.
    </div>
- type: input
  input_type: 'button'
  id: step_15_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 15
  link:
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_15_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 15
  link: https://about.gitlab.com/pricing/faq-consumption-cicd/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 5
  classes: []
---
