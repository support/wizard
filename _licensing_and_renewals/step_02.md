---
step: 2
message:
input:
  id: tf_6244777789084
  type: select
  label: What area does this pertain to?
  options:
  - step: 5
    value: lnr_category_purchasing_issues
    name: Making a purchase
  - step: 9
    value: lnr_category_qsr
    name: Quarterly subscription reconciliations (QSR)
  - step: 6
    value: lnr_category_saas
    name: My SaaS (gitlab.com) subscription
  - step: 7
    value: lnr_category_trial
    name: GitLab trial
  - step: 26
    value: lnr_category_cdot
    name: Questions/Issues about the Customer portal
  - step: 8
    value: lnr_category_special_program
    name: EDU/OSS program
  - step: 10
    value: lnr_category_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 1
  classes: []
- name: next
  value: Next
  step: 5
  classes: []
- name: next
  value: Next
  step: 6
  classes: []
- name: next
  value: Next
  step: 7
  classes: []
- name: next
  value: Next
  step: 8
  classes: []
- name: next
  value: Next
  step: 9
  classes: []
- name: next
  value: Next
  step: 10
  classes: []
- name: next
  value: Next
  step: 26
  classes: []
---
