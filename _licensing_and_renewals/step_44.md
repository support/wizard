---
step: 44
message:
input:
  id: tf_6244763208860
  type: select
  label: What is the nature of your Customer portal question?
  options:
  - step: 54
    value: lnr_cdot_login
    name: Login related issues
  - step: 55
    value: lnr_cdot_transfer
    name: Account transfer related issues
  - step: 56
    value: lnr_cdot_contacts
    name: Adding approved contacts
  - step: 57
    value: lnr_cdot_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 54
  classes: []
- name: next
  value: Next
  step: 55
  classes: []
- name: next
  value: Next
  step: 56
  classes: []
- name: next
  value: Next
  step: 57
  classes: []
---
