---
step: 5
message:
input:
  id: tf_6244764805916
  type: select
  label: What is the nature of your purchasing question?
  options:
  - step: 11
    value: lnr_purchasing_issues_cc_issues
    name: I am encountering credit card errors when trying to make a purchase
  - step: 12
    value: lnr_purchasing_issues_non_cc_issues
    name: I don't see my namespace/group in the dropdown when making a purchase
  - step: 13
    value: lnr_purchasing_issues_non_cc_issues
    name: How is it determining the number of seats I need to purchase?
  - step: 14
    value: lnr_purchasing_issues_other
    name: I have questions about buying a SaaS subscription
  - step: 15
    value: lnr_purchasing_issues_other
    name: I have questions about buying CI Minutes
  - step: 16
    value: lnr_purchasing_issues_other
    name: I have questions about buying storage
  - step: 17
    value: lnr_purchasing_issues_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 2
  classes: []
- name: next
  value: Next
  step: 11
  classes: []
- name: next
  value: Next
  step: 12
  classes: []
- name: next
  value: Next
  step: 13
  classes: []
- name: next
  value: Next
  step: 14
  classes: []
- name: next
  value: Next
  step: 15
  classes: []
- name: next
  value: Next
  step: 16
  classes: []
- name: next
  value: Next
  step: 17
  classes: []
---
