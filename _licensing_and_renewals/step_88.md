---
step: 88
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    If you purchased your subscription directly from GitLab, you can obtain your license or cloud activation key using the information found <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/#obtain-a-subscription' target='_blank'>here</a>.
    <br />
    <br />
    Otherwise, if you purchased from an authorized partner or reseller (including AWS and GCP), you will need to submit a support ticket.
    </div>
- type: input
  input_type: 'button'
  id: step_88_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 69
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_gitlab-dedicated
  - tf_6244777789084=lnr_category_sm
  - tf_6244817812892=lnr_sm_did_not_receive
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I need to get my license or cloud activation key
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/self_managed/</li></ul></blockquote>
  value: I was not able to obtain the license. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_88_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 88
  link: https://docs.gitlab.com/ee/subscriptions/self_managed/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 78
  classes: []
---
