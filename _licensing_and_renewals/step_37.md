---
step: 37
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about our special programs can be found at:
    <ul>
      <li><a href='https://about.gitlab.com/solutions/education/' target='_blank'>GitLab for Education page</a></li>
      <li><a href='https://about.gitlab.com/solutions/education/join/' target='_blank'>Join the GitLab for Education Program</a></li>
      <li><a href='https://about.gitlab.com/handbook/marketing/community-relations/community-programs/education-program/' target='_blank'>Education Program handbook page</a></li>
      <li><a href='https://about.gitlab.com/solutions/open-source/' target='_blank'>GitLab Solutions for Open Source Projects page</a></li>
      <li><a href='https://about.gitlab.com/solutions/open-source/join/' target='_blank'>Join the GitLab for Open Source Program page</a></li>
      <li><a href='https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/' target='_blank'>Open Source Program handbook page</a></li>
    </ul>
    To get in contact with our special program teams for more information, renewals, or issues, you can:
    <ul>
      <li>Email <a href='mailto:education@gitlab.com'>education@gitlab.com</a> about the Education program</li>
      <li>Email <a href='mailto:opensource@gitlab.com'>opensource@gitlab.com</a> about the OSS program</li>
    </ul>
    </div>
- type: input
  input_type: 'button'
  id: step_37_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 37
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_6244777789084=lnr_category_special_program
  - tf_6244802892956=lnr_special_programs_sheerid
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I am unable to claim my subscription using my SheerID
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have looked over the info at https://about.gitlab.com/solutions/education/</li><li>I have looked over the info at https://about.gitlab.com/solutions/education/join</li><li>I have looked over the info at https://about.gitlab.com/handbook/marketing/community-relations/community-programs/education-program/</li><li>https://about.gitlab.com/solutions/open-source/</li><li>https://about.gitlab.com/solutions/open-source/join/</li><li>https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_37_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 37
  link: https://about.gitlab.com/solutions/
  vars:
  value: This gave me the info I needed. This has been resolved.
- type: input
  input_type: 'button'
  id: step_37_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 37
  link: Emailed special program
  vars:
  value: I will email the given address to resolve this.
input:
buttons:
- name: previous
  value: Previous
  step: 8
  classes: []
---
