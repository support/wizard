---
step: 48
message:
input:
  id: tf_6244802892956
  type: select
  label: What is the nature of your EDU/OSS program question?
  options:
  - step: 65
    value: lnr_special_programs_questions
    name: I have questions about the EDU/OSS program
  - step: 66
    value: lnr_special_programs_renew
    name: I need to renew/change my EDU/OSS subscription
  - step: 67
    value: lnr_special_programs_sheerid
    name: I am unable to claim my subscription using my SheerID
  - step: 68
    value: lnr_special_programs_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 65
  classes: []
- name: next
  value: Next
  step: 66
  classes: []
- name: next
  value: Next
  step: 67
  classes: []
- name: next
  value: Next
  step: 68
  classes: []
---
