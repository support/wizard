---
step: 23
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-warning
  content: |
    <div><img src='assets/images/warning.svg'></div>
    Click "Submit a ticket" to open the new ticket form. Note that some of the form fields will be pre-populated based on your answers so far.
- type: input
  input_type: 'button'
  id: step_23_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 23
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_saas
  - tf_subject=I need to link my subscription to my namespace
  - tf_6244777789084=lnr_category_saas
  - tf_6244816806044=lnr_saas_link_sub
  - tf_6244823221788=resold_status_reseller
  - tf_description=<blockquote><p>Zendesk Wizard Notes</p><p>I have used the wizard and confirmed a ticket is required to resolve this issue.</p></blockquote>
  value: Submit a ticket
input:
buttons:
- name: previous
  value: Previous
  step: 18
  classes: []
---
