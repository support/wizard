---
step: 71
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    A GitLab self-managed subscription uses a hybrid model. You pay for a subscription according to the <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#maximum-users' target='_blank'>maximum number</a> of users enabled during the subscription period. For instances that aren’t offline or on a closed network, the maximum number of simultaneous users in the GitLab self-managed installation is checked each quarter.
    <br />
    <br />
    If an instance is unable to generate a quarterly usage report, the existing <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#users-over-license' target='_blank'>true-up model</a> is used. Prorated charges are not possible without a quarterly usage report.
    <br />
    <br />
    More information on how we determine seat usage can be found <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#billable-users' target='_blank'>here</a>.
    <br />
    <br />
    Some other useful information for you might be <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#view-user-totals' target='_blank'>view user totals</a> and <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#export-your-license-usage' target='_blank'>exporting license usage</a>.
    </div>
- type: input
  input_type: 'button'
  id: step_71_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 71
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_sm
  - tf_6244777789084=lnr_category_sm
  - tf_6244817812892=lnr_sm_seat_usage
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I am encountering seat usage issues on my Self-Managed instance
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/gitlab_com/</li><li>I have checked https://docs.gitlab.com/ee/subscriptions/self_managed/index.html</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_71_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 71
  link: https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 46
  classes: []
---
