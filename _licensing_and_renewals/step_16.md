---
step: 16
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about storage can be found on the following pages:
    <ul>
      <li><a href='https://about.gitlab.com/pricing/licensing-faq/#can-i-buy-more-storage' target='_blank'>Licensing and subscription FAQ</a></li>
      <li><a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#add-on-subscription-for-additional-storage-and-transfer' target='_blank'>Add-on subscription for additional Storage and Transfer</a></li>
    </ul>
    However, if you need more assistance, you might want to contact our sales team.
    </div>
- type: input
  input_type: 'button'
  id: step_16_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 16
  link:
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_16_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 16
  link: https://docs.gitlab.com/ee/subscriptions/gitlab_com/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 5
  classes: []
---
