---
step: 62
message:
- type: div
  classes:
  - alert
  - alert-warning
  content: |
    <div><img src='assets/images/warning.svg'></div>
    <div>
    If license generated is not working for your instance, it could be the instance needed a bigger license or has had a license used in the past.
    <br />
    <br />
    That being the case, you would need to contact our support team regarding this matter.
    </div>
- type: input
  input_type: 'button'
  id: step_62_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 62
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_sm
  - tf_6244777789084=lnr_category_trial
  - tf_6244820160540=lnr_trial_cannot_apply
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I cannot apply a trial license to my instance
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/user/group/manage.html</li></ul></blockquote>
  value: I still cannot apply the license. I want to submit a ticket.
input:
buttons:
- name: previous
  value: Previous
  step: 47
  classes: []
---
