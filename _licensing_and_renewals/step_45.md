---
step: 45
message:
input:
  id: tf_6244815614108
  type: select
  label: What is the nature of your QSR question?
  options:
  - step: 58
    value: lnr_qsr_questions
    name: I have questions about QSR
  - step: 59
    value: lnr_qsr_dispute
    name: I want to dispute my recent QSR
  - step: 60
    value: lnr_qsr_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 58
  classes: []
- name: next
  value: Next
  step: 59
  classes: []
- name: next
  value: Next
  step: 60
  classes: []
---
