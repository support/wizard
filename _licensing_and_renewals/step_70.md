---
step: 70
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    If you are getting errors about exceeding the number of users for your license, try to solve the problem using the information <a href='https://docs.gitlab.com/ee/user/admin_area/license_file.html#users-exceed-license-limit-upon-renewal' target='_blank'>here</a> in our docs.
    <br />
    <br />
    Otherwise, please contact your GitLab Sales representative to address errors received when trying to apply your license.
    </div>
- type: input
  input_type: 'button'
  id: step_70_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 70
  link: https://docs.gitlab.com/ee/user/admin_area/license_file.html
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_70_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 70
  link: https://docs.gitlab.com/ee/user/admin_area/license_file.html
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 46
  classes: []
---
