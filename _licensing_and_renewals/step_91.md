---
step: 91
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    If you purchased your subscription directly from GitLab, there are a few ways you can get your license sent to someone else:
    <ol>
      <li>
        Self-Service:
        <ul>
          <li>Change the owner of the customers.gitlab.com account by following the instructions <a href='https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information' target='_blank'>here</a>.</li>
          <li>Then have the new owner log in to the Customers Portal and copy the key from the Manage Purchases page</li>
          <li>If you are not using Cloud Licensing, and you see only an activation key on the Manage Purchases page, please open a support ticket and ask that we send you your license file.</li>
        </ul>
      </li>
      <li>Add a new contact: Submit a support ticket asking to add a new contact to the Customers Portal account, and then have the new contact log in to the Customers Portal and copy the key from the Manage Purchases page</li>
    </ol>
    Otherwise, if you purchased from an authorized partner or reseller (including AWS and GCP), please submit a support ticket.
    </div>
- type: input
  input_type: 'button'
  id: step_91_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 91
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_gitlab-dedicated
  - tf_6244777789084=lnr_category_sm
  - tf_6244817812892=lnr_sm_resend_to_new_contact
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I want to add a new contact in the customer's portal
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have checked https://docs.gitlab.com/ee/subscriptions/</li><li>I have checked https://docs.gitlab.com/ee/subscriptions/self_managed/</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_91_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 91
  link: https://docs.gitlab.com/ee/subscriptions/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 78
  classes: []
---
