---
step: 9
message:
input:
  id: tf_6244815614108
  type: select
  label: What is the nature of your QSR question?
  options:
  - step: 39
    value: lnr_qsr_questions
    name: I have questions about QSR
  - step: 40
    value: lnr_qsr_dispute
    name: I want to dispute my recent QSR
  - step: 41
    value: lnr_qsr_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 2
  classes: []
- name: next
  value: Next
  step: 39
  classes: []
- name: next
  value: Next
  step: 40
  classes: []
- name: next
  value: Next
  step: 41
  classes: []
---
