---
step: 40
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    Disputes regarding your QSR would need to be addressed via your sales representative. If the below information does not help to address your dispute, you would want to speak to our Sales team.
    <br />
    <br />
    General information about GitLab's Quarterly Subscription Reconciliation can be found <a href='https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html' target='_blank'>here</a>.
    <br />
    <br />
    A GitLab SaaS subscription uses a concurrent (seat) model. You pay for a subscription according to the maximum number of users assigned to the top-level group or its children during the billing period. You can add and remove users during the subscription period, as long as the total users at any given time doesn’t exceed the subscription count.
    <br />
    <br />
    More information on how we determine seat usage can be found <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined' target='_blank'>here</a>.
    <br />
    <br />
    Some other useful information for you might be <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#view-seat-usage' target='_blank'>viewing seat usage</a>, <a href='https://docs.gitlab.com/ee/subscriptions/gitlab_com/#export-seat-usage' target='_blank'>exporting seat usage</a>, and <a href='https://docs.gitlab.com/ee/api/members.html#list-all-billable-members-of-a-group' target='_blank'>listing billable members of a group via the API</a> (keep in mind our API uses <a href='https://docs.gitlab.com/ee/api/index.html#pagination' target='_blank'>pagination</a>, so you might need to make multiple calls if your usage is over the standard output length).
    </div>
- type: input
  input_type: 'button'
  id: step_40_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 40
  link: https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_40_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 40
  link: https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 9
  classes: []
---
