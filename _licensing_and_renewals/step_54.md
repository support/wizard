---
step: 54
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-warning
  content: |
    <div><img src='assets/images/warning.svg'></div>
    Click "Submit a ticket" to open the new ticket form. Note that some of the form fields will be pre-populated based on your answers so far.
- type: input
  input_type: 'button'
  id: step_54_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 54
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_sm
  - tf_6244777789084=lnr_category_cdot
  - tf_6244763208860=lnr_cdot_login
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I am having issues logging into the customer portal
  - tf_description=<blockquote><p>Zendesk Wizard Notes</p><p>I have used the wizard and confirmed a ticket is required to resolve this issue.</p></blockquote>
  value: Submit a ticket
input:
buttons:
- name: previous
  value: Previous
  step: 44
  classes: []
---
