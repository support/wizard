---
step: 55
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    To transfer ownership of your Customers Portal account to someone else, please follow the steps in our <a href='https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information' target='_blank'>documentation</a> regarding how to change account owner information.
    </div>
- type: input
  input_type: 'button'
  id: step_55_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 55
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_sm
  - tf_6244777789084=lnr_category_cdot
  - tf_6244763208860=lnr_cdot_transfer
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I need to transfer ownership of my customer portal account
  - tf_description=<blockquote><p>Zendesk Wizard notes</p><ul><li>I have tried changing the owner using the info at https://docs.gitlab.com/ee/subscriptions/</li></ul></blockquote>
  value: This did not address my questions. I want to submit a ticket.
- type: input
  input_type: 'button'
  id: step_55_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 55
  link: https://docs.gitlab.com/ee/subscriptions/
  vars:
  value: I was able to change the owner of the account. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 44
  classes: []
---
