---
step: 7
message:
input:
  id: tf_6244820160540
  type: select
  label: What is the nature of your GitLab trial question?
  options:
  - step: 31
    value: lnr_trial_cannot_start
    name: I cannot start a new trial
  - step: 32
    value: lnr_trial_cannot_apply
    name: I cannot apply my trial
  - step: 33
    value: lnr_trial_questions
    name: I have questions about trials
  - step: 34
    value: lnr_trial_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 7
  classes: []
- name: next
  value: Next
  step: 31
  classes: []
- name: next
  value: Next
  step: 32
  classes: []
- name: next
  value: Next
  step: 33
  classes: []
- name: next
  value: Next
  step: 34
  classes: []
---
