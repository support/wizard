---
step: 93
message:
- type: div
  classes:
  - text-center
  - alert
  - alert-warning
  content: |
    <div><img src='assets/images/warning.svg'></div>
    Click "Submit a ticket" to open the new ticket form. Note that some of the form fields will be pre-populated based on your answers so far.
- type: input
  input_type: 'button'
  id: step_93_ticket
  classes:
  - ticket_submit
  - btn
  - btn-outline-primary
  step: 93
  link:
  vars:
  - tf_360012882099=l_and_r_product_type_gitlab-dedicated
  - tf_6244777789084=lnr_category_sm
  - tf_6244817812892=lnr_sm_other
  - tf_6244823221788=resold_status_unsure
  - tf_subject=I have questions about a self-managed license
  - tf_description=<blockquote><p>Zendesk Wizard Notes</p><p>I have used the wizard and confirmed a ticket is required to resolve this issue.</p></blockquote>
  value: Submit a ticket
input:
buttons:
- name: previous
  value: Previous
  step: 78
  classes: []
---
