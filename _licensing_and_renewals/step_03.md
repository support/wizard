---
step: 3
message:
input:
  id: tf_43970347
  type: text
  label: What version of GitLab are you currently using?
  pattern: '[0-9]+\.[0-9]+\.[0-9]+'
  placeholder: 'Ex: 15.0.1'
  input_title: This should be the GitLab instance version, in the format of XX.YY.ZZ
  step: 42
  helper:
    text:
    - The exact GitLab version you're running, in x.y.z format, such as 14.10.4. Do not include the distribution suffix after the version number (-ee). To determine your version, visit your GitLab instance help page (https://gitlab.example.com/help) or the Admin Area.
    - Please note our form will take the value you put into this field and attempt to auto-correct it to the needed format.
buttons:
- name: previous
  value: Previous
  step: 1
  classes: []
- name: next
  value: Next
  step: 42
  classes: []
---
