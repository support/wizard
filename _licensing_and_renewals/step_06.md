---
step: 6
message:
input:
  id: tf_6244816806044
  type: select
  label: What is the nature of your SaaS subscription question?
  options:
  - step: 18
    value: lnr_saas_link_sub
    name: I need help linking a subscription to a namespace
  - step: 19
    value: lnr_saas_seat_usage
    name: I need help related to seat usage
  - step: 20
    value: lnr_saas_sub_issues
    name: My subscription is not correct on my namespace
  - step: 21
    value: lnr_purchasing_issues_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 2
  classes: []
- name: next
  value: Next
  step: 18
  classes: []
- name: next
  value: Next
  step: 19
  classes: []
- name: next
  value: Next
  step: 20
  classes: []
- name: next
  value: Next
  step: 21
  classes: []
---
