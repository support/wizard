---
step: 26
message:
input:
  id: tf_6244763208860
  type: select
  label: What is the nature of your Customer portal question?
  options:
  - step: 27
    value: lnr_cdot_login
    name: Login related issues
  - step: 28
    value: lnr_cdot_transfer
    name: Account transfer related issues
  - step: 29
    value: lnr_cdot_contacts
    name: Adding approved contacts
  - step: 30
    value: lnr_cdot_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 2
  classes: []
- name: next
  value: Next
  step: 27
  classes: []
- name: next
  value: Next
  step: 28
  classes: []
- name: next
  value: Next
  step: 29
  classes: []
- name: next
  value: Next
  step: 30
  classes: []
---
