---
step: 43
message:
input:
  id: tf_6244764805916
  type: select
  label: What is the nature of your purchasing question?
  options:
  - step: 50
    value: lnr_purchasing_issues_cc_issues
    name: I am encountering credit card errors when trying to make a purchase
  - step: 51
    value: lnr_purchasing_issues_non_cc_issues
    name: How is it determining the number of seats I need to purchase?
  - step: 52
    value: lnr_purchasing_issues_other
    name: I have questions about buying a Self-Managed subscription
  - step: 53
    value: lnr_purchasing_issues_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 50
  classes: []
- name: next
  value: Next
  step: 51
  classes: []
- name: next
  value: Next
  step: 52
  classes: []
- name: next
  value: Next
  step: 53
  classes: []
---
