---
step: 63
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about GitLab trials can be found on the following pages:
    <ul>
      <li><a href='https://about.gitlab.com/pricing/licensing-faq/#how-do-i-start-a-trial' target='_blank'>How do I start a trial?</a></li>
      <li><a href='https://about.gitlab.com/free-trial/#what-is-included-in-my-free-trial-what%E2%80%99s-excluded' target='_blank'>What is included in my free trial? What’s excluded?</a></li>
      <li><a href='https://about.gitlab.com/free-trial/#what-happens-after-my-free-trial-ends' target='_blank'>What happens after my free trial ends?</a></li>
      <li><a href='https://about.gitlab.com/free-trial/#is-a-credit-debit-card-required-for-a-free-trial' target='_blank'>Is a credit/debit card required for a free trial?</a></li>
    </ul>
    If you are looking to get more information about GitLab trials, you might want to contact our sales team.
    </div>
- type: input
  input_type: 'button'
  id: step_63_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 63
  link: https://about.gitlab.com/free-trial/
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_63_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 63
  link: https://about.gitlab.com/free-trial/
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 47
  classes: []
---
