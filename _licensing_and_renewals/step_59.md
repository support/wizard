---
step: 59
message:
- type: div
  classes:
  - alert
  - alert-info
  content: |
    <div><img src='assets/images/information.svg'></div>
    <div>
    General information about GitLab's Quarterly Subscription Reconciliation can be found <a href='https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html' target='_blank'>here</a>.
    <br />
    <br />
    A GitLab self-managed subscription uses a hybrid model. You pay for a subscription according to the <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#maximum-users' target='_blank'>maximum number</a> of users enabled during the subscription period. For instances that aren’t offline or on a closed network, the maximum number of simultaneous users in the GitLab self-managed installation is checked each quarter.
    <br />
    <br />
    If an instance is unable to generate a quarterly usage report, the existing <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#users-over-license' target='_blank'>true-up model</a> is used. Prorated charges are not possible without a quarterly usage report.
    <br />
    <br />
    More information on how we determine seat usage can be found <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#billable-users' target='_blank'>here</a>.
    <br />
    <br />
    Some other useful information for you might be <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#view-user-totals' target='_blank'>view user totals</a> and <a href='https://docs.gitlab.com/ee/subscriptions/self_managed/index.html#export-your-license-usage' target='_blank'>exporting license usage</a>.
    <br />
    <br />
    If the above information does not fully address your concerns, you will need to to contact your GitLab Sales representative to dispute your QSR.
    </div>
- type: input
  input_type: 'button'
  id: step_59_ticket
  classes:
  - contact_sales
  - btn
  - btn-outline-primary
  step: 59
  link: https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
  vars:
  value: This did not address my questions. Please send me to the sales contact form.
- type: input
  input_type: 'button'
  id: step_59_resolved
  classes:
  - mark_resolved
  - btn
  - btn-primary
  step: 59
  link: https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
  vars:
  value: This gave me the info I needed. This has been resolved.
input:
buttons:
- name: previous
  value: Previous
  step: 45
  classes: []
---
