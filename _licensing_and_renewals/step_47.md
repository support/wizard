---
step: 47
message:
input:
  id: tf_6244820160540
  type: select
  label: What is the nature of your GitLab trial question?
  options:
  - step: 61
    value: lnr_trial_cannot_start
    name: I cannot start a new trial
  - step: 62
    value: lnr_trial_cannot_apply
    name: I cannot apply my trial
  - step: 63
    value: lnr_trial_questions
    name: I have questions about trials
  - step: 64
    value: lnr_trial_other
    name: Something else
buttons:
- name: previous
  value: Previous
  step: 42
  classes: []
- name: next
  value: Next
  step: 61
  classes: []
- name: next
  value: Next
  step: 62
  classes: []
- name: next
  value: Next
  step: 63
  classes: []
- name: next
  value: Next
  step: 64
  classes: []
---
