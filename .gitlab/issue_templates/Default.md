## Zendesk Wizard Issue

**NOTE**: This is maintained via our [Standard Change Management process](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_management/#standard-change-management)

### During creation checklist

- [ ] Link the issue that brought about this change if it exists
  - If it was a support-team-meta issue, use `/relate LINK_TO_ISSUE` below and remove the other options
  - If it was another readiness issue and this new issue _blocks_ the previous issue, use `/blocks LINK_TO_ISSUE` below and remove the other options
  - If it was another readiness issue and this new issue _is blocked_ by the previous issue, use `/blocked_by LINK_TO_ISSUE` below and remove the other options
- [ ] Ensure the assignees below are the correct [DRIs](https://handbook.gitlab.com/handbook/support/readiness/operations/division_of_responsibilities/) for this issue
- [ ] Ensure the [change level](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/) set below is correct
  - Use ~"support-ops-change::1" for [Criticality 1](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-1)
  - Use ~"support-ops-change::2" for [Criticality 2](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-2)
  - Use ~"support-ops-change::3" for [Criticality 3](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-3)
  - Use ~"support-ops-change::4" for [Criticality 4](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-4)
- [ ] Ensure the Zendesk instance label used below is correct
  - Use ~"Zendesk::Global" for issues pertaining to [Zendesk Global](https://gitlab.zendesk.com/agent)
  - Use ~"Zendesk::US-Federal" for issues pertaining to [Zendesk US Federal](https://gitlab-federal-support.zendesk.com/agent/)
  - Remove the entire label for issues pertaining to [Zendesk Global](https://gitlab.zendesk.com/agent) or [Zendesk US Federal](https://gitlab-federal-support.zendesk.com/agent/)

### Post-creation checklist

- [ ] Determine the estimated time needed to work this issue to completion and set the milestone based on which deployment it will fall into. If you need assistance with this, please ping a Support Readiness, Operations manager.
  - **Remember the cutoff date is 5 business days before the next deployment date**
  - Example, if you plan on this going live 2023-12-01, you should apply the milestone "Support Ops Deployment: 2023-12-01"
  - If an exception is required, please see [our documentation](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_management/#exceptions)
- [ ] Communicate to the original requester (either in this issue or the original one) what the slated date for deployment would be
- [ ] Set the label on this issue to ~"SupportOps::Doing" 
- [ ] Perform all sandbox level setup where possible, notating the changes made in this issue
  - Make sure to add time spent to the issue!
- [ ] Perform a test-suite based off the changes
  - Make sure to add time spent to the issue!
- [ ] Ping the original requester on this issue to review the test-suite results
- [ ] Set the label for this issue to ~"SupportOps::Blocked" 
- [ ] Once the original requester has approved the results of the test-suite, create a MR for this change
  - Ensure the MR closes this issue when merged
  - Ensure the MR is reviewed and approved by others in Support Readiness before merging it
  - Ensure the MR is linked to this issue
- [ ] Set the label on this issue to ~"SupportOps::Completed" 

---

<!-- DO NOT EDIT BELOW THIS LINE -->

---

<!-- Link other issues -->

/relate LINK_TO_ISSUE
/blocks LINK_TO_ISSUE
/blocked_by LINK_TO_ISSUE

<!-- Labels -->

<!-- Support Ops progress label. Do not change this -->

/label ~"SupportOps::To Do"

<!-- Support Ops category label. Do not change this --> 

/label ~"Support-Ops-Category::Forms" 

<!-- Change level label -->

/label ~"support-ops-change::4" 

<!-- Zendesk instance label -->

/label ~"Zendesk::Global" 

<!-- Assignees -->

/assign @jcolyer
/assign @dtragjasi