## Zendesk Wizard Merge Request

**NOTE**: This is maintained via our [Standard Change Management process](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_management/#standard-change-management)

### During creation checklist

- [ ] Ensure this is linked to the Readiness issue
- [ ] Ensure this is set to close the issue in question
- [ ] Ensure the assignees and reviewers below are the correct [DRIs](https://handbook.gitlab.com/handbook/support/readiness/operations/division_of_responsibilities/) for this issue
- [ ] Ensure the [change level](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/) set below is correct
  - Use ~"support-ops-change::1" for [Criticality 1](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-1)
  - Use ~"support-ops-change::2" for [Criticality 2](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-2)
  - Use ~"support-ops-change::3" for [Criticality 3](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-3)
  - Use ~"support-ops-change::4" for [Criticality 4](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/change_criticalities/#criticality-4)
- [ ] Ensure the Zendesk instance label used below is correct
  - Use ~"Zendesk::Global" for issues pertaining to [Zendesk Global](https://gitlab.zendesk.com/agent)
  - Use ~"Zendesk::US-Federal" for issues pertaining to [Zendesk US Federal](https://gitlab-federal-support.zendesk.com/agent/)
  - Remove the entire label for issues pertaining to [Zendesk Global](https://gitlab.zendesk.com/agent) or [Zendesk US Federal](https://gitlab-federal-support.zendesk.com/agent/)
- [ ] Ensure this has the correct milestone set
- [ ] Ensure the MR is set to `Squash commits when merging`
- [ ] Ensure the MR is set to delete the source branch after merging

### Post-creation checklist

- [ ] Get a review and approval from someone other than yourself on this MR
- [ ] If this is a ~"support-ops-change::1" or ~"support-ops-change::2", please cc `@jcolyer` and `@lyle` on this issue
- [ ] Set the label on this issue to ~"SupportOps::Completed" 
- [ ] Merge the MR into the `master` branch

---

<!-- DO NOT EDIT BELOW THIS LINE -->

---

<!-- Labels -->

<!-- Support Ops progress label. Do not change this -->

/label ~"SupportOps::Doing"

<!-- Support Ops category label. Do not change this --> 

/label ~"Support-Ops-Category::Forms" 

<!-- Change level label -->

/label ~"support-ops-change::4" 

<!-- Zendesk instance label -->

/label ~"Zendesk::Global" 

<!-- Assignees -->

/assign @jcolyer
/assign @dtragjasi

/<!-- Reviewers -->

/assign_reviewer @jcolyer
/assign_reviewer @dtragjasi

<!-- Sets milestone -->

/milestone %"Support Ops Deployment: YYYY-MM-DD" 

<!-- Closes other issue -->

Closed #xxx