# licensing_and_renewals pathing

1. What type of product are you working with?
   * `GitLab.com (SaaS)` goes to step 2
   * `Self-Managed` goes to step 3
   * `GitLab Dedicated` goes to step 4
2. What area does this pertain to?
   * `Making a purchase` goes to step 5
   * `Quarterly subscription reconciliations (QSR)` goes to step 9
   * `My SaaS (gitlab.com) subscription` goes to step 6
   * `GitLab trial` goes to step 7
   * `Questions/Issues about the Customer portal` goes to step 26
   * `EDU/OSS program` goes to step 8
   * `Something else` goes to step 10
3. Asks for GitLab version and leads to step 42
4. Asks for GitLab version and leads to step 75
5. What is the nature of your purchasing question?
   * `I am encountering credit card errors when trying to make a purchase` goes to step 11
   * `I don't see my namespace/group in the dropdown when making a purchase` goes to step 12
   * `How is it determining the number of seats I need to purchase?` goes to step 13
   * `I have questions about buying a SaaS subscription` goes to step 14
   * `I have questions about buying CI Minutes` goes to step 15
   * `I have questions about buying storage` goes to step 16
   * `Something else` goes to step 17
6. What is the nature of your SaaS subscription question?
   * `I need help linking a subscription to a namespace` goes to step 18
   * `I need help related to seat usage` goes to step 19
   * `My subscription is not correct on my namespace` goes to step 20
   * `Something else` goes to step 21
7. What is the nature of your GitLab trial question?
   * `I cannot start a new trial` goes to step 31
   * `I cannot apply my trial` goes to step 32
   * `I have questions about trials` goes to step 33
   * `Something else` goes to step 34
8. What is the nature of your EDU/OSS program question?
   * `I have questions about the EDU/OSS program` goes to step 35
   * `I need to renew/change my EDU/OSS subscription` goes to step 36
   * `I am unable to claim my subscription using my SheerID` goes to step 37
   * `Something else` goes to step 38
9. What is the nature of your QSR question?
   * `I have questions about QSR` goes to step 39
   * `I want to dispute my recent QSR` goes to step 40
   * `Something else` goes to step 41
10. **Ticket submission required** (end of path)
11. **Ticket submission required** (end of path)
12. Doc link to https://docs.gitlab.com/ee/user/group/manage.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
13. Doc link to https://docs.gitlab.com/ee/subscriptions/gitlab_com/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
14. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
15. Doc link to https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
16. Doc link to https://docs.gitlab.com/ee/subscriptions/gitlab_com/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
17. **Ticket submission required** (end of path)
18. How was the subscription purchased?
    * `I purchased via an authorized partner or reseller (including AWS and GCP)` goes to step 22
    * `I am an authorized partner or reseller` goes to step 23
    * `I purchased directly from GitLab` goes to step 24
    * `I am not sure` goes to step 25
19. Doc link to https://docs.gitlab.com/ee/subscriptions/gitlab_com/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
20. Doc link to https://docs.gitlab.com/ee/subscriptions
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
21. **Ticket submission required** (end of path)
22. **Ticket submission required** (end of path)
23. **Ticket submission required** (end of path)
24. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
25. **Ticket submission required** (end of path)
26. What is the nature of your Customer portal question?
    * `Login related issues` goes to step 27
    * `Account transfer related issues` goes to step 28
    * `Adding approved contacts` goes to step 29
    * `Something else` goes to step 30
27. **Ticket submission required** (end of path)
28. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
29. **Ticket submission required** (end of path)
30. **Ticket submission required** (end of path)
31. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
32. Doc link to https://docs.gitlab.com/ee/user/group/manage.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
33. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
34. **Ticket submission required** (end of path)
35. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
36. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
37. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
38. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
39. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
40. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
41. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
42. What area does this pertain to?
    * `Making a purchase` goes to step 43
    * `Questions/Issues about the Customer portal` goes to step 44
    * `Quarterly Subscription Reconciliations (QSR) related matters` goes to step 45
    * `Self-managed license related matters` goes to step 46
    * `GitLab trial` goes to step 47
    * `EDU/OSS program` goes to step 48
    * `Something else` goes to step 49
43. What is the nature of your purchasing question?
    * `I am encountering credit card errors when trying to make a purchase` goes to step 50
    * `How is it determining the number of seats I need to purchase?` goes to step 51
    * `I have questions about buying a Self-Managed subscription` goes to step 52
    * `Something else` goes to step 53
44. What is the nature of your Customer portal question?
    * `Login related issues` goes to step 54
    * `Account transfer related issues` goes to step 55
    * `Adding approved contacts` goes to step 56
    * `Something else` goes to step 57
45. What is the nature of your QSR question?
    * `I have questions about QSR` goes to step 58
    * `I want to dispute my recent QSR` goes to step 59
    * `Something else` goes to step 60
46. What is the nature of your Self-managed license question?
    * `I have not received my license` goes to step 69
    * `I am getting errors applying my license or activation code` goes to step 70
    * `Seat usage related matters` goes to step 71
    * `I need my license sent to someone else` goes to step 72
    * `Request to opt out of cloud licensing` goes to step 73
    * `Something else` goes to step 74
47. What is the nature of your GitLab trial question?
    * `I cannot start a new trial` goes to step 61
    * `I cannot apply my trial` goes to step 62
    * `I have questions about trials` goes to step 63
    * `Something else` goes to step 64
48. What is the nature of your EDU/OSS program question?
    * `I have questions about the EDU/OSS program` goes to step 65
    * `I need to renew/change my EDU/OSS subscription` goes to step 66
    * `I am unable to claim my subscription using my SheerID` goes to step 67
    * `Something else` goes to step 68
49. **Ticket submission required** (end of path)
50. **Ticket submission required** (end of path)
51. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
52. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
53. **Ticket submission required** (end of path)
54. **Ticket submission required** (end of path)
55. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
56. **Ticket submission required** (end of path)
57. **Ticket submission required** (end of path)
58. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
59. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
60. Doc link to https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
61. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
62. **Ticket submission required** (end of path)
63. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
64. **Ticket submission required** (end of path)
65. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
66. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
67. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
68. Doc link to https://about.gitlab.com/free-trial/
    * Can be marked as resolved by info or
    * Can be marked as resolved by emailing the team or
    * **Ticket submission required** (end of path)
69. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/
    * Can be marked as resolved by info or
    * **Ticket submission required** (end of path)
70. Doc link to https://docs.gitlab.com/ee/user/admin_area/license_file.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
71. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
72. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
73. Info about SCL
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
74. **Ticket submission required** (end of path)
75. What area does this pertain to?
    * `Making a purchase` goes to step 76
    * `Questions/Issues about the Customer portal` goes to step 77
    * `Self-managed license related matters` goes to step 78
    * `Something else` goes to step 79
76. What is the nature of your purchasing question?
    * `I am encountering credit card errors when trying to make a purchase` goes to step 80
    * `How is it determining the number of seats I need to purchase?` goes to step 81
    * `I have questions about buying a Self-Managed subscription` goes to step 82
    * `Something else` goes to step 83
77. What is the nature of your Customer portal question?
    * `Login related issues` goes to step 84
    * `Account transfer related issues` goes to step 85
    * `Adding approved contacts` goes to step 86
    * `Something else` goes to step 87
78. What is the nature of your Self-managed license question?
    * `I have not received my license` goes to step 88
    * `I am getting errors applying my license or activation code` goes to step 89
    * `Seat usage related matters` goes to step 90
    * `I need my license sent to someone else` goes to step 91
    * `Request to opt out of cloud licensing` goes to step 92
    * `Something else` goes to step 93
79. **Ticket submission required** (end of path)
80. **Ticket submission required** (end of path)
81. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
82. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
83. **Ticket submission required** (end of path)
84. **Ticket submission required** (end of path)
85. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
86. **Ticket submission required** (end of path)
87. **Ticket submission required** (end of path)
88. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/
    * Can be marked as resolved by info or
    * **Ticket submission required** (end of path)
89. Doc link to https://docs.gitlab.com/ee/user/admin_area/license_file.html
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
90. Doc link to https://docs.gitlab.com/ee/subscriptions/self_managed/index.html
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
91. Doc link to https://docs.gitlab.com/ee/subscriptions/
    * Can be marked as resolved or
    * **Ticket submission required** (end of path)
92. Info about SCL
    * Can be marked as resolved or
    * **Sent to Sales form** (end of path)
93. **Ticket submission required** (end of path)
