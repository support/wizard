function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
  return false;
};
function setDefaults() {
  $('input').each(function() {
    if ($(this).attr('type') == 'button') { return }
    if (getUrlParameter($(this).attr('name'))) {
      $(this).val(getUrlParameter($(this).attr('name')));
    }
  });
  $('select').each(function() {
    $(this).val(getUrlParameter($(this).attr('name')));
  });
}
function validate(object) {
  $(object).parent().parent().find('input[name="next"]').hide();
  var step = $(object).data('step');
  if ($(object).val() != '') {
    if ($(object).prop('validity')?.valid) {
      $(object).css('border-color', 'green');
      $(object).parent().parent().find('input[data-next-step="' + step + '"]').show();
    } else {
      $(object).css('border-color', 'red');
    }
  } else {
    $(object).css('border-color', 'rgb(206, 212, 218)');
  }
}
function adjustVersion(object) {
  var match = $(object).val().match(/\d+\.\d+\.\d+/);
  if (match) {
    $(object).val(match[0]);
  }
}
$(document).ready(function() {
  var current = getUrlParameter('step') || 1;
  $('fieldset').hide();
  $('fieldset[data-step="' + current + '"]').show();
  setDefaults();
  $(".next").click(function(){
    current_step = $(this).parent().attr('data-step')
    next_step = $(this).attr('data-next-step');
    $('fieldset[data-step="' + next_step + '"]').show();
    $(this).parent().parent().hide();
  });
  $(".previous").click(function(){
    current_step = $(this).parent().attr('data-step')
    prev_step = $(this).attr('data-prev-step');
    $('fieldset[data-step="' + prev_step + '"]').show();
    $(this).parent().parent().hide();
  });
  $('select').change(function() {
    $(this).parent().parent().find('input[name="next"]').hide()
    var step = $(this).find(':selected').data('step');
    $(this).parent().parent().find('input[data-next-step="' + step + '"]').show();
  });
  $('input').on('keyup change', function() {
    validate(this);
  });
  $('#tf_43970347').keyup(function() {
    adjustVersion(this);
    validate(this);
  });
  $('input.mark_resolved').click(function() {
    var data = {
      Step: $(this).attr('data-step'),
      Link: $(this).attr('data-link'),
      Status: 'resolved'
    }
    $.ajax({
      method: 'POST',
      dataType: 'json',
      url: 'https://script.google.com/macros/s/AKfycbxXkA49tSLMOdsxMzq28nKBfxCubUurWE1gZtyyc0eMpsFpO4OoDDjUyxeN4HrB6hbU/exec',
      data: data
    });
    alert('Thank you for letting us know! Have a great day!');
  });
  $('input.ticket_submit').click(function() {
    var ajax_data = {
      Step: $(this).attr('data-step'),
      Status: 'ticket made'
    }
    $.ajax({
      method: 'POST',
      dataType: 'json',
      url: 'https://script.google.com/macros/s/AKfycbxXkA49tSLMOdsxMzq28nKBfxCubUurWE1gZtyyc0eMpsFpO4OoDDjUyxeN4HrB6hbU/exec',
      data: ajax_data
    });
    var data = $(this).data('vars');
    if ($('#tf_43970347').val() != '') {
      data += '&tf_43970347=' + $('#tf_43970347').val();
    }
    var link = 'https://gitlab.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000071293&' + data;
    var win = window.open(link, '_blank');
    if (win) {
      win.focus();
    } else {
      alert("Popup blocked by browser. Please navigate to " + link)
    }
  });
  $('input.contact_sales').click(function() {
    var ajax_data = {
      Step: $(this).attr('data-step'),
      Status: 'sent to sales'
    }
    $.ajax({
      method: 'POST',
      dataType: 'json',
      url: 'https://script.google.com/macros/s/AKfycbxXkA49tSLMOdsxMzq28nKBfxCubUurWE1gZtyyc0eMpsFpO4OoDDjUyxeN4HrB6hbU/exec',
      data: ajax_data
    });
    var link = 'https://about.gitlab.com/sales/'
    var win = window.open(link, '_blank');
    if (win) {
      win.focus();
    } else {
      alert("Popup blocked by browser. Please navigate to " + link)
    }
  });
  $('#start_over').click(function() {
    location.reload();
  });
});
