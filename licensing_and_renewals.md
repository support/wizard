---
layout: default
---
<form id='lnr_form' novalidate='' action='#'>
  {% for s in site.licensing_and_renewals %}
    <fieldset data-step='{{ s.step }}'>
      <div class='form-group'>
        {% if s.message %}
          {% for h in s.message %}
            {% if h.type == 'div' or h.type == 'p' %}
              <{{ h.type }} class='{{ h.classes | join: ' ' }}'>
               {{ h.content }}
              </{{ h.type }}>
             {% elsif h.type == 'br' or h.type == 'hr' %}
               <{{ h.type }}>
             {% elsif h.type == 'input' %}
               <p class='text-center'><input type='{{ h.input_type }}' id='{{ h.id }}' name='{{ h.id }}' class='{{ h.classes | join: ' ' }}' data-step='{{ h.step }}' data-link='{{ h.link }}' data-vars="{{ h.vars | join: '&' }}" value='{{ h.value }}'></p>
             {% endif %}
          {% endfor %}
          {% endif %}
        {% if s.input %}
          {% if s.input.type == 'select' %}
            <label for='{{ s.input.id }}' class='form-label fw-bold'>{{ s.input.label }}</label>
            <select class='form-control form-select' id='{{ s.input.id }}' name='{{ s.input.id }}'>
            <option value=''></option>
            {% for o in s.input.options %}
              <option data-step='{{ o.step }}' value='{{ o.value }}'>{{ o.name }}</option>
            {% endfor %}
            </select>
          {% elsif s.input.type == 'text' %}
            <label for='{{ s.input.id }}'>{{ s.input.label }}</label>
            <input type='text' class='form-control' id='{{ s.input.id }}' name='{{ s.input.id }}' pattern='{{ s.input.pattern }}' placeholder='{{ s.input.placeholder }}' title='{{ s.input.input_title }}' data-step='{{ s.input.step }}' />
            {% if s.input.helper %}
              {% for t in s.input.helper.text %}
                <p class='row text-center'>
                  <small class='form-text text-muted px-4'>{{ t }}</small>
                </p>
              {% endfor %}
            {% endif %}
          {% endif %}
        {% endif %}
      </div>
      <div class='offset-9 mt-2'>
      {% for b in s.buttons %}
        {% if b.name == 'previous' %}
          <input type='button' name='{{ b.name }}' data-prev-step='{{ b.step }}' class='previous btn btn-outline-primary {{ b.classes | join: ' ' }}' value='{{ b.value }}'>
        {% elsif b.name == 'next' %}
          <input type='button' name='{{ b.name }}' data-next-step='{{ b.step }}' class='next btn btn-primary {{ b.classes | join: ' ' }}' value='{{ b.value }}'>
        {% endif %}
      {% endfor %}
      </div>
    </fieldset>
  {% endfor %}
</form>
